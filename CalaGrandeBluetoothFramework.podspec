Pod::Spec.new do |s|

  s.name         = "CalaGrandeBluetoothFramework"
  s.version      = "2.0.1"
  s.summary      = "Framework that handles communication with Cala Grande peripheral device."
  s.description  = "Framework handles communication with Cala Grande peripheral device. It will scan, connect and transfer log data from the peripheral to central. Binary files will be generated from received data."
  s.homepage     = "https://bitbucket.org/DomBabic/calagrandebluetoothframework"
  s.license      = { :type => 'MIT', :text => './LICENSE.md' }
  s.author       = { "Dominik Babić" => "dominik.babic@codecons.com" }
  s.platform     = :ios, '8.0'
s.source       = { :git => "https://DomBabic@bitbucket.org/DomBabic/calagrandebluetoothframework.git", :tag => "2.0.1" }
  s.source_files  = "CalaGrandeBluetoothFramework", "CalaGrandeBluetoothFramework/**/*.{h,m}"
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }
  s.swift_version = '4.0'

end
