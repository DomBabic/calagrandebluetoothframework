//
//  Date+Extension.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 16/04/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

import Foundation

extension Date {
    func getOffsetFromUTC() -> Int {
        return Calendar.current.timeZone.secondsFromGMT(for: self)
    }
}
