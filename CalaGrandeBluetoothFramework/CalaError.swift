//
//  CalaError.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 21/03/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

import Foundation

public struct CalaError: Error {
    public var title: String
    public var description: String
}
