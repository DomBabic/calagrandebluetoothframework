//
//  UIntConversion.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 23/03/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

import Foundation

class BytesConversion {
    public static func convertUInt32ToUInt8Array(bytes: UInt32) -> [UInt8] {
        var littleEndian = bytes.littleEndian
        let count = MemoryLayout<UInt32>.size
        
        let bytePtr = withUnsafePointer(to: &littleEndian) {
            $0.withMemoryRebound(to: UInt8.self, capacity: count) {
                UnsafeBufferPointer(start: $0, count: count)
            }
        }
        
        return Array(bytePtr)
    }
}
