//
//  ArrayUInt8+Extension.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 28/03/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

import Foundation

extension Array where Element == UInt8 {
    func calculateCrc32() -> String {
        
        var crc: UInt32 = 0xffffffff
        for i in 0..<self.count {
            crc = crc ^ UInt32(self[i])
            for _ in 0...7 {
                let b = crc & 0x80000000
                crc = (crc & 0x7fffffff) << 1
                if (b != 0) {
                    crc = crc^0x4C11DB7
                }
            }
        }
        
        var formattedCrc = String(NSString(format:"%02X", crc))
        
        while formattedCrc.count < 8 {
            formattedCrc = "0" + formattedCrc
        }
        
        return formattedCrc
    }
}
