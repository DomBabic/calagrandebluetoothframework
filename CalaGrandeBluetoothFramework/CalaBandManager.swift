//
//  CalaGrandeConnectionManager.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 19/03/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

@objc
public class CalaBandManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    private weak var calaBandManagerDelegate: CalaBandManagerDelegate!
    
    /*
     * Instance of central and peripheral unit.
     */
    private var calaCentral: CBCentralManager!
    private var calaPeripheral: CBPeripheral?
    
    /*
     * Scan/pause timer and intervals.
     */
    private var timer: Timer!
    private let scanInterval = 2
    private let pauseInterval = 10
    
    /*
     * BLE device serial number array list
     */
    private var advertisingDevices: [String: CBPeripheral] = [:]
    
    /*
     * Grande BLE services and characteristics.
     */
    private var calaPeripheralServices: [CBService]?
    private var calaPeripheralCharacteristics: [CBCharacteristic]?
    
    /*
     * Variables used in creation of dictionary for json
     */
    private var fwVersion: String? = nil
    private var offset: Int = -1
    private var peripheralSerialNumber: String? = nil
    private var timestamp: String? = nil
    private var createdAt: Date? = nil
    private var logType: String? = nil
    
    /*
     * Flow control flags.
     */
    private var shouldDeleteLogs = false
    private var firstPass = true
    private var fetchingTime = false
    private var firstSegment = true
    private var transferingSegment = false
    private var imageAcknowledge = false
    private var fetchingImageChecksum = false
    
    /*
     * Retry segment transfer counter, max value = 3
     */
    private var segmentResendCounter: Int = 0
    
    /*
     * Segment indicators for size and memory address.
     */
    private var imageSize: UInt32 = 0
    private var segmentSize: UInt32 = 0
    private var address: UInt32 = 0
    
    /*
     * Buffers for transfered bytes.
     */
    private var image: [UInt8] = [UInt8]()
    private var segmentFragments: [UInt8] = [UInt8]()
    
    /*
     * Types of Grande BLE logs.
     */
    private var logTypeIterator = [Data(bytes: [0x00]),                 //Patient log
                                   Data(bytes: [0x01]),                 //Device log
                                   Data(bytes: [0x02])].makeIterator()  //Imu log
    
    /*
     * Log to which device is currently set.
     */
    private var currentLogType: Data?
    
    /*
     * Filepaths to generated log binary files.
     */
    private var logPaths: [String] = [String]()
    
    public init(with delegate: CalaBandManagerDelegate) {
        super.init()
        
        calaBandManagerDelegate = delegate
        currentLogType = logTypeIterator.next()
        
        calaCentral = CBCentralManager(delegate: self,
                                       queue: nil,
                                       options: [CBCentralManagerOptionShowPowerAlertKey: true])
    }
    
    //MARK: Methods handling scanning for the peripherals
    public func setupForScan(with deletion: Bool) {
        shouldDeleteLogs = deletion
        scanForPeripheral()
    }
    
    @objc private func scanForPeripheral() {
        
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(scanInterval),
                                     target: self,
                                     selector: #selector(pauseScan),
                                     userInfo: nil,
                                     repeats: false)
        
        let service = CBUUID(string: Cbuuids.Service.rawValue)
        self.calaCentral.scanForPeripherals(withServices: [service], options: nil)
    }
    
    @objc private func pauseScan() {
        print("*** SCAN PAUSED ***")
        
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(pauseInterval),
                                     target: self,
                                     selector: #selector(scanForPeripheral),
                                     userInfo: nil,
                                     repeats: false)
        
        calaCentral.stopScan()
        calaBandManagerDelegate.onCentralDiscovered(peripherals: Array(advertisingDevices.keys))
    }
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        var calaError: CalaError? = nil
        
        switch central.state {
        case .unknown:
            calaError = CalaError(title: "Central state: Unknown!",
                                  description: "The state of BLE Manager is unknown.")
        case .resetting:
            calaError = CalaError(title: "Central state: Resetting!",
                                  description: "The BLE Manager is resetting; a state update is pending...")
        case .unsupported:
            calaError = CalaError(title: "Central state: Unsupported!",
                                  description: "This device does not support Bluetooth Low Energy!")
        case .unauthorized:
            calaError = CalaError(title: "Central state: Unauthorized!",
                                  description: "This app is not authorized to use Bluetooth Low Energy.")
        case .poweredOff:
            calaError = CalaError(title: "Central state: Powered Off!",
                                  description: "Bluetooth on this device is currently powered off!")
        case .poweredOn:
            calaBandManagerDelegate.onCentralPoweredOn()
        }
        
        if calaError != nil {
            calaBandManagerDelegate.onErrorOccurred(calaError!)
        }
    }
    
    
    //Upon discovering peripheral devices try to find the one containing Grande in its name
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        guard let peripheralName = advertisementData[CBAdvertisementDataLocalNameKey] as? String else {
            return
        }
        
        if peripheralName.contains("Grande") {
            print("Cala Grande band discovered...\nUpdating device list...")
            
            guard let serialNumber = peripheralName.split(separator: "-").last?.trimmingCharacters(in: .whitespaces) else {
                return
            }
            
            if !advertisingDevices.contains(where: { $0.key == serialNumber}) {
                advertisingDevices[serialNumber] = peripheral
            }
        }
    }
    
    public func connectToPeripheral(with serialNumber: String) {
        timer.invalidate()
        calaCentral.stopScan()
        
        guard let filteredDevices = advertisingDevices.first(where: { $0.key == serialNumber }) else {
            let error = CalaError(title: "Device not found!",
                                  description: "Could not connect to \(serialNumber)...")
            calaBandManagerDelegate.onErrorOccurred(error)
            return
        }
        
        calaPeripheral = filteredDevices.value
        calaPeripheral?.delegate = self
        
        if peripheralSerialNumber == nil {
            peripheralSerialNumber = serialNumber
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyddMM-HHmmss"
            
            createdAt = Date()
            
            timestamp = dateFormatter.string(from: createdAt!)
        }
        
        connectToPeripheral()
    }
    
    //If Cala Grande was discovered, connect to it
    private func connectToPeripheral() {
        if calaPeripheral != nil {
            calaCentral.connect(calaPeripheral!, options: nil)
        } else {
            let error = CalaError(title: "Peripheral device is nil!",
                                  description: "Value of peripheral device being attempted to connect to is set to nil.")
            calaBandManagerDelegate.onErrorOccurred(error)
        }
    }
    
    //If peripheral device was connected attempt to discover services of peripheral device
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Cala Grande band connected...")
        advertisingDevices.removeAll()
        calaBandManagerDelegate.onPeripheralConnected()
        calaPeripheral?.discoverServices(nil)
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print(error.debugDescription)
        calaBandManagerDelegate.onErrorOccurred(CalaError(title: "Connecting failed!",
                                                          description: "Central failed to connect to peripheral."))
        
        scanForPeripheral()
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        guard let services = peripheral.services else {
            let calaError = CalaError(title: "Error discovering services!",
                                      description: "No services found for peripheral device.")
            
            calaBandManagerDelegate.onErrorOccurred(calaError)
            return
        }
        
        print("Discovered Cala Grande band services...")
        
        self.calaPeripheralServices = services
        calaBandManagerDelegate.onDiscoveredServices()
        discoverCharacteristicsForServices()
    }
    
    //Upon discovering services, for CBUUID_SERVICE attempt to discover characteristics
    private func discoverCharacteristicsForServices() {
        for service in calaPeripheralServices! {
            if service.uuid.uuidString == Cbuuids.Service.rawValue {
                calaPeripheral!.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    //Once characteristics are discovered store them and enable notifications for each of the characteristics
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if error != nil {
            let calaError = CalaError(title: "Error discovering characteristics!",
                                      description: "Peripheral ran into an error while discovering characteristics for service:\n\(service)")
            
            calaBandManagerDelegate.onErrorOccurred(calaError)
            return
        }
        
        guard let characteristics = service.characteristics else {
            let calaError = CalaError(title: "Characteristics not found!",
                                      description: "Service characteristics returned nil.")
            calaBandManagerDelegate.onErrorOccurred(calaError)
            return
        }
        
        //TODO: When adding new services, characteristics need to be appended if not already added.
        calaPeripheralCharacteristics = characteristics
        
        //Enable notifications for characteristic
        for characteristic in calaPeripheralCharacteristics! {
            peripheral.setNotifyValue(true, for: characteristic)
        }
        
        print("Discovered Cala Grande band characteristics...")
        calaBandManagerDelegate.onDiscoveredCharacteristics()
        
        if firstPass, let version_char = getCharacteristic(with: Cbuuids.VersionChar) {
            calaPeripheral?.readValue(for: version_char)
        } else if !firstPass {
            startLogTransfer()
        }
    }
    
    //Prepare for segment transfer per log type
    private func startLogTransfer() {
        
        guard let log = currentLogType else {
            print("Logs transfer completed...")
            
            if let epoch_char = getCharacteristic(with: Cbuuids.EpochChar) {
                let currentDate = Date()
                let newDeviceTime: UInt32 = UInt32(currentDate.timeIntervalSince1970) + UInt32(currentDate.getOffsetFromUTC())
                print("New device time: \(newDeviceTime)...")
                
                let data = Data(bytes: BytesConversion.convertUInt32ToUInt8Array(bytes: newDeviceTime))
                
                calaPeripheral?.writeValue(data, for: epoch_char, type: .withResponse)
            }
            
            return
        }
        
        switch log {
        case Data(bytes: [0x00]):
            logType = "patient"
        case Data(bytes: [0x01]):
            logType = "device"
        default:
            logType = "imu"
        }
        
        writeValueForLogType(data: log)
    }
    
    private func writeValueForLogType(data: Data) {
        for characteristic in calaPeripheralCharacteristics! {
            if characteristic.uuid.uuidString == Cbuuids.LogTypeChar.rawValue {
                calaPeripheral?.writeValue(data, for: characteristic, type: .withResponse)
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if let _ = error {
            let calaError = CalaError(title: "Error writting value for characteristic.",
                                      description: "Peripheral failed to write value for characteristic: \n\(characteristic.debugDescription)")
            
            calaBandManagerDelegate.onErrorOccurred(calaError)
            return
        }
        
        switch characteristic.uuid.uuidString {
        case Cbuuids.CommandChar.rawValue:
            
            if !transferingSegment {
                guard let img_char = getCharacteristic(with: Cbuuids.ImageSizeChar) else {
                    return
                }
                
                peripheral.readValue(for: img_char)
                break
            }
            
            if imageAcknowledge {
                writeImageToFile()
                
                resetAllProperties()
                connectToPeripheral()
                break
            }
            
        case Cbuuids.LogTypeChar.rawValue:
            guard let cmd_char = getCharacteristic(with: Cbuuids.CommandChar) else {
                return
            }
            
            peripheral.writeValue(Data(bytes: [ControlBytes.BYTE_CMD_START_IMAGE.rawValue]), for: cmd_char, type: .withResponse)
            
        case Cbuuids.AddressChar.rawValue:
            startSegmentTransfer()
            
        case Cbuuids.EpochChar.rawValue:
            let json = generateJsonObject()
            resetAllProperties()
            timer.invalidate()
            calaCentral.stopScan()
            calaBandManagerDelegate.onAllLogTransfersCompleted(logPaths, json)
            
        default:
            break
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard let data = characteristic.value else {
            return
        }
        
        switch characteristic.uuid.uuidString {
        case Cbuuids.VersionChar.rawValue:
            if let version = getFirmwareVersion(from: data) {
                print("GrandeBLE Version: \(version)...")
                fwVersion = version
            }
            
            if let cmd_char = getCharacteristic(with: Cbuuids.CommandChar) {
                fetchingTime = true
                calaPeripheral?.writeValue(Data(bytes: [ControlBytes.BYTE_CMD_GET_TIME.rawValue]), for: cmd_char, type: .withResponse)
            }
            
        case Cbuuids.TimeChar.rawValue:
            fetchingTime = false
            
            let seconds = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
            let date = Date(timeIntervalSince1970: TimeInterval(seconds))
            print("Cala Grande band time: \(date)...")
            
            calaBandManagerDelegate.onDeviceTimeRead(date.description)
            
            let currentDate = Date()
            offset = Int(date.timeIntervalSince1970 - currentDate.timeIntervalSince1970)
            print("Offset: \(offset)...")
            
            if firstPass {
                firstPass = false
                startLogTransfer()
            }
            
        case Cbuuids.ImageSizeChar.rawValue:
            imageSize = UInt32(data: data)
            
            print("Cala Grande image size - \(imageSize) bytes...")
            
            if imageSize > 0 {
                readValueForSegmentCharacteristic()
            } else {
                writeImageToFile()
                resetAllProperties()
                connectToPeripheral()
            }
            
        case Cbuuids.SegmentSizeChar.rawValue:
            segmentSize = UInt32(data: data)
            print("Cala Grande segment size - \(segmentSize) bytes...")
            
            if firstSegment {
                address = 0
                firstSegment = false
            } else if !firstSegment && !transferingSegment {
                address += segmentSize
            }
            
            print("Cala Grande segment start address - \(address)...")
            
            guard let addr_char = getCharacteristic(with: Cbuuids.AddressChar) else {
                return
            }
            
            let data = Data(bytes: BytesConversion.convertUInt32ToUInt8Array(bytes: address))
            calaPeripheral?.writeValue(data, for: addr_char, type: .withResponse)
            transferingSegment = true
            
        case Cbuuids.TransferChar.rawValue:
            
            segmentFragments.append(contentsOf: data)
            
        case Cbuuids.ChecksumChar.rawValue:
            
            let receivedChecksum = data.hexEncodedString(options: .upperCase)
            var segmentBuffer: [UInt8] = []
            
            if fetchingImageChecksum {
                let calculatedImageCrc = image.calculateCrc32()
                
                if calculatedImageCrc.elementsEqual(receivedChecksum) {
                    guard let cmd_char = getCharacteristic(with: Cbuuids.CommandChar) else {
                        return
                    }
                    
                    print("Image checksum verified...")
                    
                    //TODO: IMG_SUCCESS once it is confirmed to be working
                    imageAcknowledge = true
                    var bytes = [ControlBytes.BYTE_CMD_END_IMAGE_FAIL.rawValue]
                    
                    if (shouldDeleteLogs) {
                        bytes = [ControlBytes.BYTE_CMD_END_IMAGE_SUCCESS.rawValue]
                    }
                    
                    calaPeripheral?.writeValue(Data(bytes: bytes), for: cmd_char, type: .withResponse)
                    return
                }
            }
            
            print("Received checksum - \(receivedChecksum)...")
            
            if imageSize > segmentSize {
                
                for (index, value) in segmentFragments.enumerated() {
                    if index < segmentSize.hashValue {
                        segmentBuffer.append(value)
                    }
                }
                
                let calculatedCrc = segmentBuffer.calculateCrc32()
                print("Calculated checksum - \(calculatedCrc)...")
                
                if calculatedCrc.elementsEqual(receivedChecksum) {
                    print("Checksum verified...")
                    
                    image.append(contentsOf: segmentBuffer)
                    
                    calaBandManagerDelegate.onSegmentTransfered(image.count, of: imageSize.hashValue)
                    
                    segmentResendCounter = 0
                    imageSize -= segmentSize
                    transferingSegment = false
                    readValueForSegmentCharacteristic()
                } else {
                    retrySegmentTransfer()
                }
            } else {
                for (index, value) in segmentFragments.enumerated() {
                    if index < imageSize {
                        segmentBuffer.append(value)
                    }
                }
                
                let calculatedCrc = segmentBuffer.calculateCrc32()
                print("Calculated checksum - \(calculatedCrc)...")
                
                if calculatedCrc.elementsEqual(receivedChecksum) {
                    print("Checksum verified...")
                    
                    segmentResendCounter = 0
                    imageSize = 0
                    transferingSegment = false
                    
                    image.append(contentsOf: segmentBuffer)
                    
                    guard let cmd_char = getCharacteristic(with: Cbuuids.CommandChar) else {
                        return
                    }
                    
                    fetchingImageChecksum = true
                    calaPeripheral?.writeValue(Data(bytes: [ControlBytes.BYTE_CMD_VERIFY_IMG.rawValue]), for: cmd_char, type: .withResponse)
                } else {
                    retrySegmentTransfer()
                }
            }
            
            segmentFragments = [UInt8]()
            
        default:
            break
        }
        
    }
    
    func getFirmwareVersion(from data: Data) -> String? {
        let versionString = String(data: data, encoding: String.Encoding.utf8)?.trimmingCharacters(in: .controlCharacters)
        
        return versionString
    }
    
    //Get required characteristic to write to.
    private func getCharacteristic(with uuid: Cbuuids) -> CBCharacteristic? {
        
        let filter = CBUUID(string: uuid.rawValue)
        
        return calaPeripheralCharacteristics!.filter({ $0.uuid == filter }).first
    }
    
    //Read value for segment size characteristic
    private func readValueForSegmentCharacteristic() {
        guard let seg_char = getCharacteristic(with: Cbuuids.SegmentSizeChar) else {
            return
        }
        
        calaPeripheral?.readValue(for: seg_char)
    }
    
    private func startSegmentTransfer() {
        guard let cmd_char = getCharacteristic(with: Cbuuids.CommandChar) else {
            return
        }
        
        calaPeripheral?.writeValue(Data(bytes: [ControlBytes.BYTE_CMD_START_SEG.rawValue]), for: cmd_char, type: .withResponse)
    }
    
    private func retrySegmentTransfer() {
        if segmentResendCounter < 3 {
            segmentResendCounter += 1
            
            startSegmentTransfer()
        } else {
            let calaError = CalaError(title: "Failed to verify checksum.", description: "Repeatedly failed to verify checksum for received segment. Aborting operation!")
            calaBandManagerDelegate.onErrorOccurred(calaError)
        }
    }
    
    //Write image to file
    private func writeImageToFile() {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let filePath = "\(documentsDirectory)/\(peripheralSerialNumber!)-\(timestamp!)-\(logType!).bin"
        
        let pointer = UnsafeBufferPointer(start:image, count:image.count)
        let data = Data(buffer:pointer)
        
        do {
            try data.write(to: URL(fileURLWithPath: filePath))
            print(filePath)
            logPaths.append(filePath)
        } catch {
            let calaError = CalaError(title: "Error writting to file!", description: "Log[\(logType!)] could not be written to file, error writting file.")
            calaBandManagerDelegate.onErrorOccurred(calaError)
        }
    }
    
    private func generateJsonObject() -> NSMutableDictionary {
        let formatter = Foundation.DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let dateString = formatter.string(from: createdAt!)
        
        let json: NSMutableDictionary = [
            "patientId": NSNull(),
            "deviceMAC": peripheralSerialNumber ?? NSNull(),
            "clinicId": NSNull(),
            "fwVersion": fwVersion ?? NSNull(),
            "logType": "Device",
            "createdAt": dateString,
            "tzOffset": offset,
            "source": NSNull()
        ]
        
        return json
    }
    
    //Reset properties prior to moving onto next log
    private func resetAllProperties() {
        firstSegment = true
        transferingSegment = false
        imageAcknowledge = false
        fetchingImageChecksum = false
        imageSize = 0
        segmentSize = 0
        address = 0
        segmentResendCounter = 0
        
        segmentFragments = []
        image = []
        
        disconnectPeripheral()
        
        currentLogType = logTypeIterator.next()
    }
    
    //Disconnect peripheral when done
    private func disconnectPeripheral() {
        if calaPeripheral != nil {
            calaCentral.cancelPeripheralConnection(calaPeripheral!)
        }
    }
}
