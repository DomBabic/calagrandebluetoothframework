//
//  CalaBandManagerDelegate.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 21/03/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

import Foundation

public protocol CalaBandManagerDelegate: class {
    func onCentralPoweredOn()
    func onCentralDiscovered(peripherals: [String])
    
    func onPeripheralConnected()
    
    func onDiscoveredServices()
    func onDiscoveredCharacteristics()
    
    func onSegmentTransfered(_ bytes: Int, of totalBytes: Int)
    
    func onErrorOccurred(_ calaError: CalaError)
    
    func onAllLogTransfersCompleted(_ logPaths: [String], _ json: NSMutableDictionary)
    func onDeviceTimeRead(_ timeFormat: String)
}
