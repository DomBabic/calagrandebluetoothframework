//
//  ControlBytes.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 19/03/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

import Foundation

enum ControlBytes: UInt8 {
    case BYTE_CMD_RESET = 0x00
    case BYTE_CMD_START_IMAGE = 0x01
    case BYTE_CMD_START_SEG = 0x02
    case BYTE_CMD_VERIFY_IMG = 0x03
    case BYTE_CMD_END_IMAGE_SUCCESS = 0x04
    case BYTE_CMD_END_IMAGE_FAIL = 0x05
    case BYTE_CMD_GET_TIME = 0x06
}
