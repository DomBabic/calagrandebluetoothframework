//
//  UUIDS.swift
//  CalaGrandeBluetoothFramework
//
//  Created by Dominik Babić on 19/03/2018.
//  Copyright © 2018 CalaHealth. All rights reserved.
//

import Foundation

enum Cbuuids: String {
    case Service = "0000BB00-1212-EFDE-1523-785FEABCD123"
    
    case CommandChar = "0000BB01-1212-EFDE-1523-785FEABCD123"
    case StatusChar = "0000BB02-1212-EFDE-1523-785FEABCD123"
    case LogTypeChar = "0000BB03-1212-EFDE-1523-785FEABCD123"
    case SegmentSizeChar = "0000BB04-1212-EFDE-1523-785FEABCD123"
    case AddressChar = "0000BB05-1212-EFDE-1523-785FEABCD123"
    case ImageSizeChar = "0000BB06-1212-EFDE-1523-785FEABCD123"
    case ChecksumChar = "0000BB07-1212-EFDE-1523-785FEABCD123"
    case TransferChar = "0000BB08-1212-EFDE-1523-785FEABCD123"
    case EpochChar = "0000BB09-1212-EFDE-1523-785FEABCD123"
    case VersionChar = "0000BB10-1212-EFDE-1523-785FEABCD123"
    case TimeChar = "0000BB11-1212-EFDE-1523-785FEABCD123"
}
